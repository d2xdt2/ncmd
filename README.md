# Nagios Crypto Miner Detector

This Nagios plugin detects CPU crypto miners by looking for a constant load over a 15 minutes interval.

A tolerance in procent must be the first argument, which is used to termine when the 3 Linux load avg are
close enough to each other to be considered to be the same.

```
Example: ./ncmd 10
```

* On hosts with low CPU usage, small tolerances such as 10, seams to work well.
* On hosts with CPU bursts, higher tolerances might be needed to compensate for the CPU peeks.

